

Code used for the experiments in Model-Agnostic Meta-Learning for Relation Classification with Limited Supervision.

It is based on Yuhao's repository: https://github.com/yuhaozhang/tacred-relation

This version requires Python 3 and PyTorch 0.1.12

Train with: `python train.py`



