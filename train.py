"""
Train a model on TACRED.
"""

import os
from datetime import datetime
import time
import numpy as np
import random
import argparse
import subprocess
from tqdm import tqdm
from copy import deepcopy
from shutil import copyfile
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable

from data.loader import DataLoader
from data.meta_loader import DataLoader as MetaDataLoader
from model.rnn import RelationModel
from eval import evaluate
from utils import scorer, constant, helper
from utils.vocab import Vocab
from data.tacred_split import preprocess

parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', type=str, default='dataset/tacred')
parser.add_argument('--vocab_dir', type=str, default='dataset/vocab')
parser.add_argument('--emb_dim', type=int, default=300, help='Word embedding dimension.')
parser.add_argument('--ner_dim', type=int, default=30, help='NER embedding dimension.')
parser.add_argument('--pos_dim', type=int, default=30, help='POS embedding dimension.')
parser.add_argument('--hidden_dim', type=int, default=100, help='RNN hidden state size.')
parser.add_argument('--num_layers', type=int, default=2, help='Num of RNN layers.')
parser.add_argument('--dropout', type=float, default=0.5, help='Input and RNN dropout rate.')
parser.add_argument('--word_dropout', type=float, default=0.07, help='The rate at which randomly set a word to UNK.') # set to 0.04
parser.add_argument('--topn', type=int, default=50, help='Only finetune top N embeddings.')
parser.add_argument('--lower', dest='lower', action='store_true', help='Lowercase all words.')
parser.add_argument('--no-lower', dest='lower', action='store_false')
parser.set_defaults(lower=False)

parser.add_argument('--attn', dest='attn', action='store_true', help='Use attention layer.')
parser.add_argument('--no-attn', dest='attn', action='store_false')
parser.set_defaults(attn=True)
parser.add_argument('--attn_dim', type=int, default=100, help='Attention size.')
parser.add_argument('--pe_dim', type=int, default=30, help='Position encoding dimension.')

parser.add_argument('--lr', type=float, default=1.0, help='Applies to SGD and Adagrad.')
parser.add_argument('--lr_decay', type=float, default=0.9)
parser.add_argument('--optim', type=str, default='sgd', help='sgd, adagrad, adam or adamax.')
parser.add_argument('--num_epoch', type=int, default=30)
parser.add_argument('--batch_size', type=int, default=50)
parser.add_argument('--max_grad_norm', type=float, default=5.0, help='Gradient clipping.')
parser.add_argument('--log_step', type=int, default=20, help='Print log every k steps.')
parser.add_argument('--log', type=str, default='logs.txt', help='Write training log to file.')
parser.add_argument('--save_epoch', type=int, default=5, help='Save model checkpoints every k epochs.')
parser.add_argument('--save_dir', type=str, default='./saved_models', help='Root dir for saving models.')
parser.add_argument('--id', type=str, default='', help='Model ID under which to save models.')
parser.add_argument('--info', type=str, default='', help='Optional info for the experiment.')

parser.add_argument('--seed', type=int, default=1234)
parser.add_argument('--cuda', type=bool, default=torch.cuda.is_available())
parser.add_argument('--cpu', action='store_true', help='Ignore CUDA.')



# META-LEARNING PARAMETERS

parser.add_argument('--inner_batch_size', action='store', type=int, default=50)
parser.add_argument('--inner_iters', action='store', type=int,default=-1)  #
parser.add_argument('--meta_step_size', action='store', type=int, default=1)  #
parser.add_argument('--meta_step_size_final', action='store', type=int, default=0)  #
parser.add_argument('--meta_batch_size', action='store', type=int,default=5)  #
parser.add_argument('--meta_iters', action='store', type=int, default=150) #
parser.add_argument('--eval_inner_iters', action='store', type=int, default=5)
parser.add_argument('--eval_interval', action='store', type=int, default=5)
parser.add_argument('--weight_decay_rate', action='store', type=int, default=1)



args = parser.parse_args()

torch.manual_seed(args.seed)
np.random.seed(args.seed)
random.seed(1234)
if args.cpu:
    args.cuda = False
elif args.cuda:
    torch.cuda.manual_seed(args.seed)

# make opt
opt = vars(args)
opt['num_class'] = len(constant.LABEL_TO_ID)

EXPERIMENT_NAME = "METATRAIN_SUPERTEST"


for iii in range(10):
    FRACTIONS = [0.001]
    for FRACTION in FRACTIONS:
        print("Training FRACTION : ",FRACTION)
        TIME = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        BASEDIR = os.path.join("dataset",EXPERIMENT_NAME,str(FRACTION))
        vocab_dir = os.path.join(BASEDIR,"vocab")
        RUNDIR = os.path.join("dataset",EXPERIMENT_NAME,str(FRACTION),TIME)

        opt["save_dir"] = RUNDIR
        opt["vocab_dir"] = vocab_dir
        opt["data_dir"] = BASEDIR

        if not os.path.isdir(vocab_dir):
            os.makedirs(vocab_dir)
        if not os.path.isdir(RUNDIR):
            os.makedirs(RUNDIR)

        #PREPROCESS DATA
        preprocess(BASEDIR,FRACTION)
        completed_process = subprocess.run(["python","prepare_vocab.py",BASEDIR,opt["vocab_dir"],"--glove_dir","dataset/glove"])


        # load vocab
        vocab_file = opt['vocab_dir'] + '/vocab.pkl'
        vocab = Vocab(vocab_file, load=True)
        opt['vocab_size'] = vocab.size
        emb_file = opt['vocab_dir'] + '/embedding.npy'
        emb_matrix = np.load(emb_file)
        assert emb_matrix.shape[0] == vocab.size
        assert emb_matrix.shape[1] == opt['emb_dim']

        # load data
        print("Loading data from {} with batch size {}...".format(opt['data_dir'], opt['batch_size']))
        train_batch = MetaDataLoader(opt['data_dir'] + '/train.json', opt['batch_size'], opt, vocab, evaluation=False)
        dev_batch = MetaDataLoader(opt['data_dir'] + '/dev.json', opt['batch_size'], opt, vocab, evaluation=True)

        model_id = opt['id'] if len(opt['id']) > 1 else '0' + opt['id']
        model_save_dir = opt['save_dir']
        opt['model_save_dir'] = model_save_dir
        helper.ensure_dir(model_save_dir, verbose=True)

        # save config
        helper.save_config(opt, model_save_dir + '/config.json', verbose=True)
        vocab.save(model_save_dir + '/vocab.pkl')
        file_logger = helper.FileLogger(model_save_dir + '/' + "logs_meta.txt", header="# "
                                                                                     "epoch\ttrain_loss\tdev_loss\tdev_f1")

        # print model info
        helper.print_config(opt)

        # model
        model = RelationModel(opt, emb_matrix=emb_matrix)

        id2label = dict([(v,k) for k,v in constant.LABEL_TO_ID.items()])
        dev_f1_history = []
        current_lr = opt['lr']

        global_step = 0
        global_start_time = time.time()
        format_str = '{}: step {}/{} (Meta Iter {}/{}), loss = {:.6f} ({:.3f} sec/batch), lr: {:.6f}'
        max_steps = opt['meta_batch_size'] * opt['meta_iters']

        # start training
        for m_iter in tqdm(range(1, opt['meta_iters'] + 1),unit="Meta-Iters"):
            frac_done = (m_iter - 1) / opt['meta_iters']
            cur_meta_step_size = frac_done * opt['meta_step_size_final'] + (1 - frac_done) * opt['meta_step_size']

            weights_original = deepcopy(model.model.state_dict())
            new_weights = []
            train_loss = []
            for epoch in range(1, opt['meta_batch_size'] + 1):
                global_step += 1
                epoch_loss = []
                start_time = time.time()
                for i in range(5): # set to 5
                    for batch in train_batch.sample_relation_instances(num_batches=opt['inner_iters'],rand=True):
                        loss = model.update(batch)
                epoch_loss += [loss]
                if epoch_loss:
                    train_loss.extend(epoch_loss)
                if global_step % opt['log_step'] == 0:
                    duration = time.time() - start_time

                new_weights.append(deepcopy(model.model.state_dict()))
                model.model.load_state_dict({name: weights_original[name] for name in weights_original})

            train_loss = np.sum(train_loss)  #
            print("meta iter {}: Total train_loss = {:.6f}".format(m_iter, train_loss))
            file_logger.log("{}\t{:.6f}".format(m_iter, train_loss))

            ws = len(new_weights)
            fweights = {name: new_weights[0][name] / float(ws) for name in new_weights[0]}
            for i in range(1, ws):
                for name in new_weights[i]:
                    fweights[name] += new_weights[i][name] / float(ws)
            model.model.load_state_dict(
                {name: weights_original[name] + ((fweights[name] - weights_original[name]) * cur_meta_step_size) for name in
                 weights_original})

            if m_iter % opt['eval_interval'] == 0:
                # eval on dev
                print("Evaluating on dev set...")
                predictions = []
                dev_loss = 0

                prior_weights = deepcopy(model.model.state_dict())
                train_relations = set(train_batch.relations)
                for index,relation in enumerate(dev_batch.relations):
                    for batch in dev_batch.sample_relation_instances(relation_name=relation):
                        preds, _, loss = model.predict(batch)
                        predictions += preds
                        dev_loss += loss

                    model.model.load_state_dict({name: prior_weights[name] for name in prior_weights})

                ##########
                #########
                predictions = [id2label[p] for p in predictions]
                dev_p, dev_r, dev_f1 = scorer.score(dev_batch.gold(), predictions, verbose=True)

                if m_iter == 1 or dev_f1 > max(dev_f1_history, default=0):
                    # save
                    model_file = model_save_dir + '/best_model.pt'
                    model.save(model_file, m_iter)
                    print("new best model saved.")

                # lr schedule
                if len(dev_f1_history) > 100 and dev_f1 <= dev_f1_history[-1] and \
                        opt['optim'] in ['sgd', 'adagrad']:
                    current_lr *= opt['lr_decay']
                    model.update_lr(current_lr)

                dev_f1_history += [dev_f1]
                print("")

        print("Training ended with {} meta iterations.".format(m_iter))


        ###################################
        ##################################

        print("STARTING SUPERVISED TRAINING")
        # load data
        print("Loading data from {} with batch size {}...".format(opt['data_dir'], opt['batch_size']))
        train_batch = DataLoader(opt['data_dir'] + '/train.json', opt['batch_size'], opt, vocab, evaluation=False)
        dev_batch = DataLoader(opt['data_dir'] + '/dev.json', opt['batch_size'], opt, vocab, evaluation=True)


        file_logger = helper.FileLogger(model_save_dir + '/' + opt['log'], header="# epoch\ttrain_loss\tdev_loss\tdev_f1")

        # print model info
        helper.print_config(opt)

        id2label = dict([(v,k) for k,v in constant.LABEL_TO_ID.items()])
        dev_f1_history = []
        current_lr = opt['lr']

        global_step = 0
        global_start_time = time.time()
        format_str = '{}: step {}/{} (epoch {}/{}), loss = {:.6f} ({:.3f} sec/batch), lr: {:.6f}'
        max_steps = len(train_batch) * opt['num_epoch']

        # start training
        for epoch in range(1, opt['num_epoch'] + 1):
            train_loss = 0
            for i, batch in enumerate(train_batch):
                start_time = time.time()
                global_step += 1
                loss = model.update(batch)
                train_loss += loss
                if global_step % opt['log_step'] == 0:
                    duration = time.time() - start_time
                    print(format_str.format(datetime.now(), global_step, max_steps, epoch, \
                                            opt['num_epoch'], loss, duration, current_lr))

            # eval on dev
            print("Evaluating on dev set...")
            predictions = []
            dev_loss = 0
            for i, batch in enumerate(dev_batch):
                preds, _, loss = model.predict(batch)
                predictions += preds
                dev_loss += loss
            predictions = [id2label[p] for p in predictions]
            dev_p, dev_r, dev_f1 = scorer.score(dev_batch.gold(), predictions)

            train_loss = train_loss / train_batch.num_examples * opt['batch_size']  # avg loss per batch
            dev_loss = dev_loss / dev_batch.num_examples * opt['batch_size']
            print("epoch {}: train_loss = {:.6f}, dev_loss = {:.6f}, dev_f1 = {:.4f}".format(epoch, \
                                                                                             train_loss, dev_loss, dev_f1))
            file_logger.log("{}\t{:.6f}\t{:.6f}\t{:.4f}".format(epoch, train_loss, dev_loss, dev_f1))

            # save
            model_file = model_save_dir + '/checkpoint_epoch_{}.pt'.format(epoch)
            model.save(model_file, epoch)
            if epoch == 1 or dev_f1 > max(dev_f1_history):
                copyfile(model_file, model_save_dir + '/best_model.pt')
                print("new best model saved.")
            if epoch % opt['save_epoch'] != 0:
                os.remove(model_file)

            # lr schedule
            if len(dev_f1_history) > 10 and dev_f1 <= dev_f1_history[-1] and \
                    opt['optim'] in ['sgd', 'adagrad']:
                current_lr *= opt['lr_decay']
                model.update_lr(current_lr)

            dev_f1_history += [dev_f1]
            print("")

        print("Training ended with {} epochs.".format(epoch))

        print("Evaluating")
        evaluate(model_save_dir, "best_model.pt", opt["data_dir"], "test",
                 outfile=os.path.join(model_save_dir, "results.txt"))

        evaluate(model_save_dir, "best_model.pt", opt["data_dir"], "test")
