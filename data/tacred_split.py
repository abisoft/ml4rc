import os,re,json
import numpy as np
from utils.constant import LABEL_TO_ID

np.random.seed(1234) #



TRAIN_FILE_ORIGINAL = "dataset/tacred/train.original.json"
DEV_FILE_ORIGINAL = "dataset/tacred/dev.original.json"
TEST_FILE_ORIGINAL = "dataset/tacred/test.original.json"
#######
######
ALL_RELATIONS = list(LABEL_TO_ID.keys())
np.random.shuffle(ALL_RELATIONS)
######
TRAIN_RELATIONS = set(ALL_RELATIONS[:20])
DEV_RELATIONS = set(ALL_RELATIONS[20:27])
TEST_RELATIONS = set(ALL_RELATIONS[27:])
######

def preprocess(RUNDIR,TRAIN_FRACTION):
    TRAIN_FILE = os.path.join(RUNDIR,"train.json") # run dir not same as dataset/semeval in oneshot exps
    DEV_FILE = os.path.join(RUNDIR,"dev.json")
    TEST_FILE = os.path.join(RUNDIR,"test.json")

    with open(TRAIN_FILE, mode="w", encoding="utf-8") as train_fh:
        data = json.load(open(TRAIN_FILE_ORIGINAL, encoding="utf-8"))
        processed = []
        for d in data:
            random_uniform = np.random.uniform(0, 1.0)
            if random_uniform < TRAIN_FRACTION:
                processed += [d]
        json.dump(processed, train_fh)

    with open(DEV_FILE, mode="w", encoding="utf-8") as dev_fh:
        data = json.load(open(DEV_FILE_ORIGINAL, encoding="utf-8"))
        processed = []
        for d in data:
            processed += [d]
        json.dump(processed, dev_fh)

    with open(TEST_FILE, mode="w", encoding="utf-8") as test_fh:
        data = json.load(open(TEST_FILE_ORIGINAL, encoding="utf-8"))
        processed = []
        for d in data:
            processed += [d]
        json.dump(processed, test_fh)



def initial_split():
    np.random.shuffle(ALL_RELATIONS)
    print("Total number of relations::",len(ALL_RELATIONS))
    TRAIN_RELATIONS = set(ALL_RELATIONS[:20])
    DEV_RELATIONS = set(ALL_RELATIONS[20:27])
    TEST_RELATIONS = set(ALL_RELATIONS[27:])

    with open(TRAIN_FILE,mode="w",encoding="utf-8") as train_fh:
        data = json.load(open(TRAIN_FILE_ORIGINAL,encoding="utf-8"))
        processed = []
        for d in data:
            relation = d['relation']
            if relation in TRAIN_RELATIONS:
                processed += [d]
        json.dump(processed,train_fh)

    with open(DEV_FILE, mode="w", encoding="utf-8") as dev_fh:
        data = json.load(open(DEV_FILE_ORIGINAL,encoding="utf-8"))
        processed = []
        for d in data:
            relation = d['relation']
            if relation in DEV_RELATIONS:
                processed += [d]
        json.dump(processed, dev_fh)

    with open(TEST_FILE, mode="w", encoding="utf-8") as test_fh:
        data = json.load(open(TEST_FILE_ORIGINAL,encoding="utf-8"))
        processed = []
        for d in data:
            relation = d['relation']
            if relation in TEST_RELATIONS:
                processed += [d]
        json.dump(processed, test_fh)


if __name__ == "__main__":
    initial_split()